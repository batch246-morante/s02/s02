
# username = input("Please enter your name: \n")
# print(f"Hello {username}! Welcome to the Python short course!")

# num1 = input("Enter 1st number: \n")
# num2 = input("Enter 2nd number: \n")
# print(f"The sum of num1 and num2 is {num1 + num2}")
# The output is the concatenation of num1 and num2 values. The reason for this is the input() method assigns any values as strings


# To solve this, the value can be typecasted to the necessary data type.
# num1 = int(input(f"Enter 1st number: \n"))
# num2 = int(input(f"Enter 2nd number: \n"))
# print(f"The sum of num1 and num2 is {num1 + num2}")


# -------------------------------------------------
# [SECTION] If-else statements
test_num = 75

if test_num >= 60:
    print("Test passed")
    print("Congratulations!")
else:
    print("Test failed")
# Note that in Python, curly braces ({}) are not needed to distinguish the code blocks inside the if or else block. Hence, indentions
# are very important as Python uses identions to distinguish parts of code as needed

# [SECTION] If else chains can also be used to have more than 2 choices for the program
# test_num2 = int(input("Please enter the 2nd test number: \n"))
# if test_num2 > 0:
#     print("The number is positive")
# elif test_num2 == 0:
#     print("The number is zero")
# else:
#     print("The number is negative")
# Note: elif is the shorthand for "else if" in other programming languages

# print("Mini Exercise 1")
# mini1_num1 = int(input("Enter Mini Activity 1 Number: "))
# if mini1_num1 % 3 == 0 and mini1_num1 % 5 == 0:
#     print("This number is divisible by both 3 and 5")
# elif mini1_num1 % 3 == 0:
#     print("This number is divisible by 3")
# elif mini1_num1 % 5 == 0:
#     print("This number is divisible 5")
# else:
#     print("The number is not divisible by 3 nor 5")

# ---------------------------------------------------------
# [SECTION] Loops
# Python has loops that can repeat blocks of code
# >> While loops are used to execute a set of statements as long as the condition is true
i = 1
while i <= 5: # as long as our i is lesser or equal to 5, the loop will run
    print(f"Current count {i}")
    i += 1 # increment by 1 every after the while condition is satisfied


# >> for loops are used for iterating a sequence
fruits = ["apple", "banana", "cherry"]
    # variable for each fruit
for each_fruit in fruits: # fruits array
    print(each_fruit)

print("----------------------------------------")
# [SECTION] range() method
# To use the for loop to iterate through values, the range method can be used.
for x in range(6):
    print(f"The current value is {x}")
# The range() function defaults to 0 as starting value. As such, this prints from 0 to 5

print("----------------------------------------")
for x in range(1,6):
    print(f"The current value is {x}")

print("----------------------------------------")

for x in range(1,10,2):
    print(f"The current value is {x}")

print("----------------------------------------")
# [SECTION] Break statement
j = 1
while j < 6:
    print(j)
    if j == 3:
        break
    j += 1


print("----------------------------------------")
# [SECTION] Continue Statement
# The continue statement returns the control to the beginning of the while loop and continue to the next iteration
z = 1
while z < 6:
    z += 1
    if z == 3:
        continue # returns back the loop condition
    print(f"Current value after incrementation: {z}")

print("----------------------------------------")
# k = 0
# while k < 6:
#     if k == 3:
#         continue
#     k += 1
#     print(f"Current value after incrementation: {k}")

