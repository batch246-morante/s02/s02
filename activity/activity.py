num1 = int(input("Enter a number here: "))
print(num1)
if type(num1) != int:
    print("Input must be an integer")
else:
    if num1 % 4 == 0:
        print(f"{num1} is a leap year!")
    else:
        print(f"{num1} is not a leap year :(")


rowNum = int(input("Enter number of rows: "))
print(rowNum)
colNum = int(input("Enter number of columns: "))
print(colNum)

for i in range(rowNum):
    print("*" * colNum)
